import pyaes
import rsa
import pickle
import sys
import socket
import threading


class Client:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def send(self, message="info"):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        s.connect((self.ip, self.port))

        if hasattr(message, "encode"):
            message = message.encode()

        s.send(message)
        data = s.recv(1024)
        s.close()
        print("received", data)
        if data[:8] == "received:".encode():
            return True
        return False


class Server:
    def __init__(self, host, port, centralAddress):
        self.host = host
        self.port = port
        self.pubKey, self.privKey = rsa.newkeys(1024)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

        self.sendpubKey(centralAddress)

    def listen(self):
        self.sock.listen(5)
        print("[node]start server %s: waiting for connections" % str(self.port))
        while True:
            client, address = self.sock.accept()
            if address:
                print("Connected", address)
            client.settimeout(60)
            threading.Thread(target=self.listenToClient, args=(client, address)).start()

    def listenToClient(self, client, address):
        size = 4096
        while True:
            # try:
            data = client.recv(size)
            if "packet:".encode() == data[:7]:
                client.sendall("received".encode())
                client.close()

                mes = pickle.loads(data[7:])
                print("[nodes]mes----------", mes.message)
                data = mes.message.rsplit("|".encode(),1)
                print("[nodes]---------0", data[0])
                print("--------------1",data[1])
                print("---key-----", mes.cKey)

                aesKey = rsa.decrypt(mes.cKey, self.privKey)
                aesCipher = pyaes.AESModeOfOperationCTR(aesKey)
                packet = aesCipher.decrypt(data[0])
                
                # ip = data[1].split(":")
                nextt = pickle.loads(packet)
                ip = nextt.message.rsplit("|".encode(),1)
                ip = ip[1].split(":".encode())
                self.sendover((ip[0], int(ip[1])), "packet:".encode() + packet)

                return True
            '''except Exception as err:
                print("[node]serv err", err)
                client.close()
                return False'''

    def sendpubKey(self, ip):
        cl = Client(ip[0], int(ip[1]))
        r = cl.send(
            "pubKeys:".encode() + pickle.dumps(self.pubKey) + "|".encode() + pickle.dumps((self.host, self.port)))
        if r:
            print("[node]pubKey sent")

    def sendover(self, ip, data):
        cl = Client(ip[0], int(ip[1]))
        print("[node]sendover", ip)
        r = cl.send(data)
        if r:
            print("[node]sentover to ", ip[0], ip[1])
        else:
            print("[node]not sent to", ip[0], ip[1])


def padding(mes):
    return mes + ('_' * (32 - len(mes)))


class Message:
    def __init__(self, message, cKey):
        self.message = message
        self.cKey = cKey


if __name__ == "__main__":
    # ip, port, central_Address
    if len(sys.argv) != 2:
        print("[node]not enough args")
    server = Server("localhost", int(sys.argv[1]), ("localhost", int(50007)))
    server.listen()
