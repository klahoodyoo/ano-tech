import pyaes
import rsa
import pickle
import sys
import socket
import threading
from random import shuffle


# used to send to the first node in the network
class Client:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def send(self, message="info"):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        s.connect((self.ip, self.port))

        if type(message) == str:
            message = message.encode()

        s.send(message)
        data = s.recv(4096)
        s.close()
        print("[central]received client central: ", data)
        if data[:8] == "received".encode():
            return 1
        return 0


# receive the public keys of nodes and encrypt the message from client
class Server:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.nodes = list()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        self.aesKeys = ["some_encryption_key_1" + "_" * 11,
                        "some_encryption_key_2" + "_" * 11,
                        "some_encryption_key_3" + "_" * 11]

        self.messages = list()
        self.pubKeys = dict()

    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            if address:
                print("[central]Connected", address)
            client.settimeout(60)
            threading.Thread(target=self.listenToClient, args=(client, address)).start()

    def listenToClient(self, client, address):
        size = 4096
        while True:
            # try:
            data = client.recv(size)
            print("[central]data", data)
            # print("pubKey", self.pubKey)
            if "pubKeys:".encode() == data[:8]:  # received all public keys from nodes
                # print("----------------", pickle.loads(data[8:]))
                ipnode = pickle.loads(data[-40:].split("|".encode())[1])
                print("=========", ipnode)
                # print("[central]--------------", ipnode)
                self.pubKeys[ipnode] = pickle.loads(data[8:])
                client.sendall("received".encode())
                # print("Sended!")
            elif "message:".encode() == data[:8]:  # received encrypted message
                print(self.pubKeys.keys())
                mes = pickle.loads(data[8:])
                print("[central]message received", mes.message)
                self.messages.append(data[8:])
                client.sendall("received".encode())

                toSend, ip = self.ComputeMessage()

                m = pickle.loads(toSend)
                print("[central]message", m.message)
                print("[central]ip", ip)
                r = self.sendover(ip, "packet:".encode() + toSend)
                if r:
                    print("[central]Message sent to node")

            client.close()
            return True
            '''except Exception as err:
                print("serv err", err)
                client.close()
                return False'''

    def ComputeMessage(self):
        lastKey, lastValue = list(self.pubKeys.keys())[-2], self.pubKeys[list(self.pubKeys.keys())[-1]]
        copyPubKeys = list(self.pubKeys.keys())
        final = bytes()
        flag = True
        for ii, i in zip(self.aesKeys[:-1], copyPubKeys[:-1]):
            aes = pyaes.AESModeOfOperationCTR(ii.encode())
            if flag:
                cAes = aes.encrypt(self.messages[0])
                flag = False
            else:
                cAes = aes.encrypt(final)
            
            print("--------ip",i)
            print("--------key",self.pubKeys[i])

            cAes = cAes + "|".encode() + i[0].encode() + ":".encode() + str(i[1]).encode()
            cKey = rsa.encrypt(ii.encode(), self.pubKeys[i])
            final = pickle.dumps(Message(cAes, cKey), 0)

        '''aes = pyaes.AESModeOfOperationCTR(self.aesKeys[-1])
        cAes = aes.encrypt(final)
        cKey = rsa.encrypt(self.aesKeys[-1].encode(), lastValue)
        final = pickle.dumps(Message(cAes, cKey), 0)'''

        return final, lastKey

    def Stop(self):
        self.sock.close()

    def sendover(self, address, data):
        cl = Client(address[0], int(address[1]))
        return cl.send(data)


class Message:
    def __init__(self, message, cKey):
        self.message = message
        self.cKey = cKey


if __name__ == "__main__":
    central = Server("localhost", 50007)
    '''for _ in range(1, 4):
        pub, priv = rsa.newkeys(1024)
        central.pubKeys[("localhost", 50007 + _)] = pub'''

    central.listen()
