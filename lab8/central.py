import pyaes
import rsa
import pickle
import sys
import socket
import threading
from random import shuffle


# used to send to the first node in the network
class Client:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def send(self, message="info"):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        s.connect((self.ip, self.port))
        s.send(message)
        data = s.recv(4096)
        s.close()
        print("received", data)
        if data[:5] == "info:".encode():
            return data[5:]
        else:
            return 0


# receive the public keys of nodes and encrypt the message from client
class Server:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.nodes = list()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        self.aesKeys = ["some_encryption_key_1" + "_" * 11,
                        "some_encryption_key_2" + "_" * 11,
                        "some_encryption_key_3" + "_" * 11]

        self.messages = list()
        self.pubKeys = dict()

    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            if address:
                print("Connected", address)
            client.settimeout(60)
            threading.Thread(target=self.listenToClient, args=(client, address)).start()

    def listenToClient(self, client, address):
        size = 4096
        while True:
            # try:
            data = client.recv(size)
            print("data", data)
            # print("pubKey", self.pubKey)
            if "pubKey:".encode() == data[:7]:
                self.pubKeys[address] = pickle.loads(data[7:])
                client.sendall("received".encode())
                # print("Sended!")
            elif "message:".encode() == data[:8]:
                mes = pickle.loads(data[8:])
                print("message received", mes.message)
                self.messages.append(data[8:])
                client.sendall("received".encode())

                toSend, ip = self.ComputeMessage()
                m = pickle.loads(toSend)
                print(m.message)
                print(ip)
                clientNode = Client(ip[0], ip[1]).send("message:".encode() + toSend)
                if clientNode:
                    print("Message sent to node")
                else:
                    print("Not sent to next node")

            client.close()
            return True
            '''except Exception as err:
                print("serv err", err)
                client.close()
                return False'''

    def ComputeMessage(self):
        firstKey, firstValue = list(self.pubKeys.keys())[0], self.pubKeys[list(self.pubKeys.keys())[0]]
        # del self.pubKeys[firstKey]
        copyPubKeys = self.pubKeys.copy()
        # del copyPubKeys[firstKey]
        # copyPubKeys[(0,0)] = ""
        final = bytes()
        flag = True
        for ii, i in zip(self.aesKeys, copyPubKeys):
            aes = pyaes.AESModeOfOperationCTR(ii.encode())
            if flag:
                cAes = aes.encrypt(self.messages[0])
                flag = False
            else:
                cAes = aes.encrypt(final)
            if i != firstKey:
                cAes = cAes + "|".encode() + i[0].encode() + ":".encode() + str(i[1]).encode()
            
            cKey = rsa.encrypt(ii.encode(), copyPubKeys[i])
            final = pickle.dumps(Message(cAes, cKey), 0)
        return final, firstKey

    def Stop(self):
        self.sock.close()


class Message:
    def __init__(self, message, cKey):
        self.message = message
        self.cKey = cKey


if __name__ == "__main__":
    central = Server("localhost", 50007)
    for _ in range(3):
        pub, priv = rsa.newkeys(1024)
        central.pubKeys[("localhost", 50007+_)] = pub

    central.listen()
