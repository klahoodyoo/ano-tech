import dist
import socket
import threading
import socketserver

class node_request_handler(socketserver.BaseRequestHandler):

    def handle(self):
        data = str(self.request.recv(4096), 'ascii')
        print(data)
        
        cur_thread = threading.current_thread()
        response = bytes("{}: {}".format(cur_thread.name, data), 'ascii')
        self.request.sendall(response)

class client_request_handler(socketserver.BaseRequestHandler):

    def handle(self):
        data = str(self.request.recv(4096), 'ascii')
        print(data)
        cur_thread = threading.current_thread()
        response = bytes("{}: {}".format(cur_thread.name, data), 'ascii')
        self.request.sendall(response)

class node:
    def __init__(self, ip, port, public_key):
        self.node = dist.node_base(ip, port, public_key)
        self.listener = dist.listener(ip, port, node_request_handler)
    
    def start(self):
        self.listener.listen()
    
    def stop(self):
        self.listener.stop()
        
    def auth(self, ip, port):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((ip, port))
            sock.sendall(bytes("auth|{}:{}|{}".format(self.node.ip, self.node.port, self.node.public_key), 'ascii'))
            response = str(sock.recv(4096), 'ascii')
            print("Received: {}".format(response))
            sock.close()

    def disconnect(self, ip, port):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((ip, port))
            sock.sendall(bytes("logout|{}:{}|{}".format(self.node.ip, self.node.port, self.node.public_key), 'ascii'))
            response = str(sock.recv(4096), 'ascii')
            print("Received: {}".format(response))
            sock.close()

    def send(self, ip, port, message):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.connect((ip, port))
            sock.sendall(bytes("message|{}:{}|{}".format(self.node.ip, self.node.port, self.node.public_key), 'ascii'))
            response = str(sock.recv(4096), 'ascii')
            print("Received: {}".format(response))
            sock.close()

if __name__ == '__main__':
    node = node("localhost", 55604, "pkey")
    node.auth("localhost", 55555)
    node.send("localhost", 55555, "hello")
    # node.disconnect("localhost", 55555)