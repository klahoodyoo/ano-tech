import socket
import threading
import socketserver

class node_base:
    def __init__(self, ip, port, public_key):
        self.ip = ip
        self.port = port
        self.public_key = public_key

    def __str__(self):
        return self.ip + ":" + self.port

    def __unicode__(self):
        return self.ip + ":" + self.port
        
    def __repr__(self):
        return self.ip + ":" + self.port

class threaded_server(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

class listener:
    def __init__(self, ip, port, request_handler):
        self.server = threaded_server((ip, port), request_handler)
        self.server_thread = threading.Thread(target=self.server.serve_forever)
        
    def listen(self):
        self.server_thread.daemon = True
        self.server_thread.start()
    def stop(self):
        print("Shutting down...")
        self.server.shutdown()