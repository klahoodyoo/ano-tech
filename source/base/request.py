from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
import base64

"""
@author: Claudiu Lungu

"""
class EncryptAlgorithm(object):
    def __init__(self, publickey):
        self.public_key = publickey
        self.private_key =''

    def encrypt(self, msg):
        return self.public_key.encrypt(msg,padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA1()),algorithm=hashes.SHA1(),label=None))

    def decrypt(self, msg, privatekey):
        return privatekey.decrypt(msg,padding.OAEP(mgf=padding.MGF1(algorithm=hashes.SHA1()),algorithm=hashes.SHA1(),label=None))

class NodeMessage(object):
    def __init__(self, publickey):
        self.encrypter = EncryptAlgorithm(publickey)
        self.private_key = rsa.generate_private_key(public_exponent=65537,key_size=2048,backend=default_backend())
        self.public_key = publickey

    def receive(self, msg):
        print('Node is decrypting a message ', msg)
        return self.encrypter.decrypt(msg, self.private_key)
        
    def send(self, msg):
        print('Node is encrypting a message ', msg)
        return self.encrypter.encrypt(msg)

class ClientMessage(object):
    
    def __init__(self, publickey):
        self.encrypter = EncryptAlgorithm(publickey)
        self.private_key = ''
        self.public_key = publickey

    def receive(self, msg):
        print('Client is receiving a message ', msg)
        return self.encrypter.decrypt(msg, self.private_key)
    
    def send(self, msg):
        print('Client is sending a message ', msg)
        return self.encrypter.encrypt(msg)

class Dispatcher(object):
    def __init__(self,publickey):
        self.node_msg = NodeMessage(publickey)
        self.client_msg = ClientMessage(publickey)

    def receive(self, request):
        if request.type == Request.node_type:
            return self.node_msg.receive(request.msg)
        elif request.type == Request.client_type:
            return self.client_msg.receive(request.msg)
        else:
            print('cant receive the request')

    def send(self, request):
        if request.type == Request.node_type:
            return self.node_msg.send(request.msg)
        elif request.type == Request.client_type:
            return self.client_msg.send(request.msg)
        else:
            print('cant send the request')


class RequestController(object):
    """ front controller """

    def __init__(self):
        self.dispatcher = Dispatcher(rsa.generate_private_key(public_exponent=65537,key_size=2048,backend=default_backend()).public_key())

    def receive_request(self, request):
        if isinstance(request, Request):
            return self.dispatcher.receive(request)
        else:
            print('request must be a Request object')
    def send_request(self, request):
        if isinstance(request, Request):
            return self.dispatcher.send(request)
        else:
            return 'request must be a Request object'


class Request(object):
    """ request """

    node_type = 'node'
    client_type = 'client'

    def __init__(self, request, msg):
        self.msg = msg
        self.type = None
        request = request.lower()
        if request == self.node_type:
            self.type = self.node_type
        elif request == self.client_type:
            self.type = self.client_type