import dist
import pyaes
import rsa
import pickle
import sys
import socket
import threading
import socketserver
from time import time as _time, sleep as _sleep

nodes = []

class server_request_handler(socketserver.BaseRequestHandler):

    def send(self, node, message):
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            print("Sending to", node)
            sock.connect((node.ip, int(node.port)))
            sock.sendall(bytes(message, 'ascii'))
            sock.close()

    def compute(self, message):
        node, message = nodes[0], message
        return node, message

    def handle(self):
        data = str(self.request.recv(4096), 'ascii')
        print(data)
        content = data.split("|")
        if content[0] == "auth":
            address = content[1].split(":")
            nodes.append(dist.node_base(address[0], address[1], content[2]))

        if content[0] == "logout":
            address = content[1].split(":")
            for node in nodes:
                if node.ip == address[0] and node.port == address[1]:
                    nodes.remove(node)

        if content[0] == "message":  # received encrypted messageprint(self.pubKeys.keys())
            message = content[1]
            print("Message received", message)

            node, c = self.compute(message)
            self.send(node, "packet|" + c)

        print(nodes)
        cur_thread = threading.current_thread()
        response = bytes("{}: {}".format(cur_thread.name, data), 'ascii')
        self.request.sendall(response)

class server:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port
        self.listener = dist.listener(ip, port, server_request_handler)

    def start(self):
        print("Started listening at", self.ip, self.port)
        self.listener.listen()

    def stop(self):
        self.listener.stop()

    def serve(self):
        self.start()
        try:
            while True:
                _sleep(1)
        except KeyboardInterrupt:
            self.stop()

if __name__ == '__main__':
    HOST, PORT = "localhost", 55555
    server = server(HOST, PORT)
    server.serve()
