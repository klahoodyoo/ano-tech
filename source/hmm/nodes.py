import pyaes
import rsa
import pickle
import sys
import socket
import threading


class Client:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def send(self, message="info"):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        s.connect((self.ip, self.port))
        s.send(message.encode())
        data = s.recv(1024)
        s.close()
        print("received", data)
        if data[:5] == "info:".encode():
            return data[5:]
        else:
            return 0


class Server:
    def __init__(self, host, port, pubKey):
        self.host = host
        self.port = port
        self.pubKey = pubKey
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            if address:
                print("Connected", address)
            client.settimeout(60)
            threading.Thread(target=self.listenToClient, args=(client, address)).start()

    def listenToClient(self, client, address):
        size = 1024
        while True:
            try:
                data = client.recv(size)
                print("data", data)
                print("pubKey", self.pubKey)
                if "info".encode() == data:
                    client.sendall("info:".encode() + pickle.dumps(self.pubKey, 0))
                    print("Sended!")
                    return True
            except Exception as err:
                print("serv err", err)
                client.close()
                return False


def padding(mes):
    return mes + ('_' * (32 - len(mes)))


class Message:
    def __init__(self, message, cKey):
        self.message = message
        self.cKey = cKey


if __name__ == "__main__":
    pass
