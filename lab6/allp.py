from os import urandom
import rsa
from random import shuffle
import pyaes
import pickle


class RsaAlg:
    def __init__(self):
        self.publicKey, self.privKey = rsa.newkeys(1024)

    def Encrypt(self, message, pubKey):
        return rsa.encrypt(message, pubKey)

    def Decrypt(self, message, privKey):
        return rsa.decrypt(message, privKey).decode()

    def GetPubKey(self):
        return self.publicKey

    def GetPrivKey(self):
        return self.privKey


class Message:
    def __init__(self, message, cKey):
        self.message = message
        self.cKey = cKey


class Request:
    def __init__(self):
        self.message = str()
        self.ip = str()
        self.port = str()

    def SetMessage(self, mess):
        self.message = mess

    def GetMessage(self):
        return self.message

    def SetIpPort(self, ip, port):
        self.ip = ip
        self.port = port

    def GetIpPort(self):
        return self.ip, self.port


class Server(Request):
    def __init__(self):
        Request.__init__(self)
        self.state = False

    def Start(self):
        self.state = True

    def Stop(self):
        self.state = False

    def GetState(self):
        return self.state


class User:
    def SendMessage(self, Request):
        pass


def randomBytes(n):
    ll = [chr(_) for _ in range(32, 127)]
    shuffle(ll)
    return "".join(ll[:n])


def padding(text):
    n = 0
    while 1:
        if ((len(text) + n) % 16) == 0 and n <= 16:
            break
        n += 1
    return n


class Client(User):
    def __init__(self, name, message, port, privKeymes):
        self.name = name
        self.privMessage = message
        self.firstNode = str()
        self.alg = RsaAlg()
        self.aes = pyaes.AESModeOfOperationCTR(privKeymes.encode())
        self.pubKey = self.alg.GetPubKey()
        self.privKey = self.alg.GetPrivKey()
        self.address = "localhost:" + str(port)
        self.otherClientAddr = str()
        self.otherPubKey = str()
        self.privKMess = privKeymes
        self.layers = ["some_encryption_key_1" + "_" * 11,
                       "some_encryption_key_2" + "_" * 11,
                       "some_encryption_key_3" + "_" * 11]

        self.receive = str()

    def ComputeMessage(self):
        r = urandom(10)
        cAes = self.aes.encrypt(self.privMessage + "|" + self.otherClientAddr)
        cKey = self.alg.Encrypt(self.privKMess.encode(), self.otherPubKey)
        ctext = pickle.dumps(Message(cAes, cKey), 0)  # what b receives

        # print("b mes:", ctext, "len: ", len(ctext))
        # ctext = ctext + b"\x00" * padding(ctext)
        # print("aaa", len(ctext))

        for i in self.layers:
            aes = pyaes.AESModeOfOperationCTR(i.encode())
            # ctext = pickle.dumps(Message(cAes, cKey), 0)  # what b receives
            cAes = aes.encrypt(ctext)
            cKey = self.alg.Encrypt(i.encode(), self.firstNode.PubKey())
            ctext = pickle.dumps(Message(cAes, cKey), 0)  # what nodes receives

        return ctext

    def SetFirstNode(self, nodex):
        self.firstNode = nodex

    def PublicKey(self):
        return self.pubKey

    def GetInfoClient(self, client):
        self.otherClientAddr = client.address
        self.otherPubKey = client.PublicKey()

    def Send(self):
        cText = self.ComputeMessage()
        print("Compute ", cText)
        '''ctext = ctext + "|".encode() + urandom(10)
        ctext = self.alg.Encrypt(ctext, self.firstNode.PubKey())
        print("aaaaaaaa")'''
        req = Request()
        ip, port = self.firstNode.server.GetIpPort()
        req.SetIpPort(ip, port)
        req.SetMessage(cText)
        self.firstNode.Receive(req)

    def Receive(self, request):
        mes = pickle.loads(request)
        print("Client B received:", mes.message, "_____", mes.cKey)
        aesKey = self.alg.Decrypt(mes.cKey, self.privKey)
        print(aesKey)
        self.aes = pyaes.AESModeOfOperationCTR(aesKey.encode())
        dtext = self.aes.decrypt(mes.message)
        print(dtext.decode().split("|")[0])
        self.receive = dtext.decode().split("|")[0]

    def ReceivedMessage(self):
        return self.receive


class Node:
    def __init__(self, name, server, algRsa):
        self.name = name
        self.neighbours = list()
        self.nextAddr = str()
        self.server = server
        self.alg = algRsa

    def PubKey(self):
        return self.alg.GetPubKey()

    def Receive(self, request):
        req = request
        mes = pickle.loads(req.GetMessage())
        print("received:", mes.message, "_____", mes.cKey)
        aesKey = self.alg.Decrypt(mes.cKey, self.alg.GetPrivKey())
        print(aesKey)
        self.aes = pyaes.AESModeOfOperationCTR(aesKey.encode())
        dtext = self.aes.decrypt(mes.message)
        print(dtext)

        mes = pickle.loads(dtext)
        print("received:", mes.message, "_____", mes.cKey)
        aesKey = self.alg.Decrypt(mes.cKey, self.alg.GetPrivKey())
        print(aesKey)
        self.aes = pyaes.AESModeOfOperationCTR(aesKey.encode())
        dtext = self.aes.decrypt(mes.message)
        print(dtext)

        mes = pickle.loads(dtext)
        print("received:", mes.message, "_____", mes.cKey)
        aesKey = self.alg.Decrypt(mes.cKey, self.alg.GetPrivKey())
        print(aesKey)
        self.aes = pyaes.AESModeOfOperationCTR(aesKey.encode())
        dtext = self.aes.decrypt(mes.message)
        print(dtext)

        clientB.Receive(dtext)


clientB = Client("B", "", 6000, "private_key_for_A" + "_" * 15)

if __name__ == "__main__":
    from random import choice

    ports = [_ for _ in range(5006, 5016)]
    servs = [Server() for _ in ports]
    [_.SetIpPort("localhost", port) for _, port in zip(servs, ports)]
    alg = RsaAlg()
    nods = [Node("name " + str(_), serv, alg) for _, serv in zip(range(len(ports)), servs)]

    for i in nods:
        i.server.Start()

    temp = nods[:-1]
    for i in nods[:-1]:
        toTake = choice(temp)
        i.neighbours = [toTake]
        temp.remove(toTake)
    print([_.neighbours[0].name for _ in nods[:-1]])

    clientA = Client("A", "Hello B", 7000, "private_key_for_B" + "_" * 15)
    clientB = Client("B", "", 6000, "private_key_for_A" + "_" * 15)

    clientA.GetInfoClient(clientB)
    clientA.SetFirstNode(nods[-1])
    clientA.Send()
