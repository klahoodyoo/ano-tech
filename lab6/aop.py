# import rsa
# import pyaes
# import pickle
import aspectlib.contrib
import pyaes


class Client(object):
    def __init__(self, address):
        self.address = address
        self.connect()

    def connect(self):
        from random import randint
        n = randint(0, 10)
        if n < 5:
            raise OSError('OsError')
        else:
            print("All good!")
        pass

    def action(self, data):
        if data == 'error':
            raise IOError("ioError")
        elif data == 'data':
            print("Message received " + data)


import time
import logging


def retry(retries=(1, 5, 15, 30, 60), retry_on=(IOError, OSError), prepare=None):
    assert len(retries)

    @aspectlib.Aspect
    def retry_aspect(*args, **kwargs):
        durations = retries
        while True:
            try:
                yield aspectlib.Proceed
                break
            except retry_on as exc:
                if durations:
                    logging.warn(exc)
                    # print(exc)
                    time.sleep(durations[0])
                    durations = durations[1:]
                    if prepare:
                        prepare(*args, **kwargs)
                    else:
                        raise

    return retry_aspect


import unittest
from os import urandom


class Aes:
    def __init__(self, key):
        self.aes = pyaes.AESModeOfOperationCTR(key)

    def encrypt(self, message):
        return self.aes.encrypt(message)

    def decrypt(self, cmessage):
        return self.aes.decrypt(cmessage)


class MyTest(unittest.TestCase):
    def test(self):

        @aspectlib.Aspect
        def mock_stuff(self, value):
            if value == 'special':
                aesalg = pyaes.AESModeOfOperationCTR(urandom(32))
                ctext = aesalg.encrypt(value)
                yield aspectlib.Return("mocked-result ".encode() + ctext)
            else:
                yield aspectlib.Proceed

        with aspectlib.weave(Aes.encrypt, mock_stuff):
            obj = Aes(urandom(32))
            self.assertIn('mocked-result'.encode(), obj.encrypt('special'))


if __name__ == "__main__":
    # patch the Client class to have the retry functionality on all its methods
    # aspectlib.weave(Client, retry())

    # or with different retry options (reconnect before retry):
    aspectlib.weave(Client, retry(prepare=lambda self, *_: self.connect()))

    # or just for one method:
    # aspectlib.weave(Client.action, retry())

    client = Client("10.100.19.21")
    #client.action("error")
    client.connect()
    # unittest.main()
