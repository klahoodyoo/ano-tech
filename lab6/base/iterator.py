
import collections.abc

class NodeAggregate(collections.abc.Iterable):
    def __init__(self):
        self._data = None

    def __iter__(self):
        return NodeIterator(self)

class NodeIterator(nodes):
    def __init__(self, node_aggregate):
        self._node_aggregate = node_aggregate

    def __next__(self):
        if True:  # if no_elements_to_traverse:
            raise StopIteration
        return None  # return element

def main():
    node_aggregate = NodeAggregate()
    for _ in node_aggregate:
        pass


if __name__ == "__main__":
    main()
