"""
@author: Claudiu Lungu

"""
class NodeMessage(object):
    def receive(self, msg):
        print('Node is decrypting a message ', msg)
        
    def send(self, msg):
        print('Node is encrypting a message ', msg)

class ClientMessage(object):
    def receive(self, msg):
        print('Client is receiving a message ', msg)
    
    def send(self, msg):
        print('Client is sending a message ', msg)

class Dispatcher(object):
    def __init__(self):
        self.node_msg = NodeMessage()
        self.client_msg = ClientMessage()

    def receive(self, request):
        if request.type == Request.node_type:
            self.node_msg.receive(request.msg)
        elif request.type == Request.client_type:
            self.client_msg.receive(request.msg)
        else:
            print('cant receive the request')

    def send(self, request):
        if request.type == Request.node_type:
            self.node_msg.send(request.msg)
        elif request.type == Request.client_type:
            self.client_msg.send(request.msg)
        else:
            print('cant send the request')


class RequestController(object):
    """ front controller """

    def __init__(self):
        self.dispatcher = Dispatcher()

    def receive_request(self, request):
        if isinstance(request, Request):
            self.dispatcher.receive(request)
        else:
            print('request must be a Request object')
    def send_request(self, request):
        if isinstance(request, Request):
            self.dispatcher.send(request)
        else:
            print('request must be a Request object')


class Request(object):
    """ request """

    node_type = 'node'
    client_type = 'client'

    def __init__(self, request, msg):
        self.msg = msg
        self.type = None
        request = request.lower()
        if request == self.node_type:
            self.type = self.node_type
        elif request == self.client_type:
            self.type = self.client_type