"""
Server class
"""
from __future__ import print_function
import request

__author__ = "Claudiu Lungu"

class State(object):

    def check_state(self):
        print(u"Checking... server state is %s" % (self.description))

class OnlineState(State):
    def __init__(self, server):
        self.server = server
        self.online = True
        self.description = "Server online"

    def toggle_onoff(self):
        print(u"Switching to offline")
        self.server.state = self.server.offlinestate

class Offline(State):
    def __init__(self, server):
        self.server = server
        self.online = False
        self.description = "Server offline"

    def toggle_onoff(self):
        print(u"Switching to online")
        self.server.state = self.server.onlinestate

class Server:

    def __init__(self):
        self.onlinestate = OnlineState(self)
        self.offlinestate = Offline(self)
        self.state = self.offlinestate
        self.request_controller = request.RequestController()

    def toggle_onoff(self):
        self.state.toggle_onoff()

    def check_state(self):
        self.state.check_state()

if __name__ == '__main__':
    server = Server()
    actions = [server.check_state] + [server.toggle_onoff] + [server.check_state]

    for action in actions:
        action()

    server.request_controller.send_request(request.Request('node', 'message 1'))
    server.request_controller.send_request(request.Request('client', 'message 1'))
    server.request_controller.send_request(request.Request('server', 'message 1'))
    server.request_controller.send_request('client')

    server.request_controller.receive_request(request.Request('node', 'message 2'))
    server.request_controller.receive_request(request.Request('client', 'message 2'))
    server.request_controller.receive_request(request.Request('server', 'message 2'))
    server.request_controller.receive_request('client')  