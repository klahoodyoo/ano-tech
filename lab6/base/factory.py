import server

class NodeFactory():
    def makeNode(self):
        return server.Server()

if __name__ == '__main__':
    node_factory = NodeFactory()
    server1 = node_factory.makeNode()
    server2 = node_factory.makeNode()

    server1.check_state()
    server2.check_state()

    server1.toggle_onoff()

    server1.check_state()
    server2.check_state()

