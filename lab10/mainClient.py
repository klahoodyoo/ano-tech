import pyaes
import rsa
from os import urandom
import pickle
import sys
import socket
import threading


class Client:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def send(self, message="info".encode()):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        s.connect((self.ip, self.port))
        s.send(message)
        data = s.recv(1024)
        s.close()
        print("received", data)
        if data[:5] == "info:".encode():
            return data[5:]
        elif data == "received".encode():
            return data
        else:
            return ""


class Server:
    def __init__(self, host, port, pubKey,privKey):
        self.host = host
        self.port = port
        self.pubKey = pubKey
        self.privKey = privKey
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def listen(self):
        self.sock.listen(5)
        print("Waiting for connections")
        while True:
            client, address = self.sock.accept()
            if address:
                print("[client]Connected", address)
            client.settimeout(60)
            threading.Thread(target=self.listenToClient, args=(client, address)).start()

    def listenToClient(self, client, address):
        size = 1024
        while True:
            # try:
            data = client.recv(size)
            print("[client]data", data)
            print("[client]pubKey", self.pubKey)
            if "info".encode() == data:
                client.sendall("info:".encode() + pickle.dumps(self.pubKey, 0))
                print("[client]Sended!")
                return True
            elif "packet:".encode() == data[:7]:
                client.sendall("received".encode())
                mes = pickle.loads(data[7:])
                print("---------mes",mes.message)
                print("---------key",mes.cKey)
                aesKey = rsa.decrypt(mes.cKey, self.privKey)
                print("-------key", aesKey)
                aesCipher = pyaes.AESModeOfOperationCTR(padding(aesKey.decode()).encode())
                packet = aesCipher.decrypt(mes.message.rsplit("|".encode())[0])
                print(packet)
                return True
            client.close()
            '''except Exception as err:
                print("[client]serv err", err)
                client.close()
                return False'''


def padding(mes):
    return mes + ('_' * (32 - len(mes)))


class Message:
    def __init__(self, message, cKey):
        self.message = message
        self.cKey = cKey


class ClientUI:
    def __init__(self, name, message, privKeymes):
        self.name = name
        self.privMessage = message
        self.aes = pyaes.AESModeOfOperationCTR(padding(privKeymes).encode())
        self.pubKey, self.privKey = rsa.newkeys(1024)
        # self.address = "localhost:" + str(port)
        self.otherClientAddr = str()
        self.otherPubKey = str()
        self.privKMess = privKeymes
        self.layers = ["some_encryption_key_1" + "_" * 11,
                       "some_encryption_key_2" + "_" * 11,
                       "some_encryption_key_3" + "_" * 11]

        self.receive = str()

        self.server = str()

    def GetPublicKey(self, ip, port):
        self.otherClientAddr = ip+":"+str(port)
        self.otherPubKey = Client(ip, port).send()
        print("[client]otherPublicKey", pickle.loads(self.otherPubKey))

    def Server(self, ip, port):
        self.server = Server(ip, int(port), self.pubKey,self.privKey)

    def StartServ(self):
        self.server.listen()

    def ComputeMessage(self):
        r = urandom(10)
        cAes = self.aes.encrypt(self.privMessage)
        cAes = cAes + "|".encode() + self.otherClientAddr.encode()
        cKey = rsa.encrypt(self.privKMess.encode(), pickle.loads(self.otherPubKey))
        ctext = pickle.dumps(Message(cAes, cKey), 0)  # what b receives

        # print("b mes:", ctext, "len: ", len(ctext))
        # ctext = ctext + b"\x00" * padding(ctext)
        # print("aaa", len(ctext))

        '''for i in self.layers:
            aes = pyaes.AESModeOfOperationCTR(i.encode())
            # ctext = pickle.dumps(Message(cAes, cKey), 0)  # what b receives
            cAes = aes.encrypt(ctext)
            cKey = self.alg.Encrypt(i.encode(), self.firstNode.PubKey())
            ctext = pickle.dumps(Message(cAes, cKey), 0)  # what nodes receives
        '''
        return ctext

    def SendCentral(self, ip, port, data):
        response = Client(ip, port).send(data)
        if len(response):
            print("Send to central server")
        else:
            print("Not sent to central server")

    def PublicKey(self):
        return self.pubKey

    def GetInfoClient(self, client):
        self.otherClientAddr = client.address
        self.otherPubKey = client.PublicKey()

    def Send(self):
        cText = self.ComputeMessage()
        print("Compute ", cText)
        '''ctext = ctext + "|".encode() + urandom(10)
        ctext = self.alg.Encrypt(ctext, self.firstNode.PubKey())
        print("aaaaaaaa")'''
        req = Request()
        ip, port = self.firstNode.server.GetIpPort()
        req.SetIpPort(ip, port)
        req.SetMessage(cText)

    def Receive(self, request):
        mes = pickle.loads(request)
        print("Client B received:", mes.message, "_____", mes.cKey)
        aesKey = self.alg.Decrypt(mes.cKey, self.privKey)
        print(aesKey)
        self.aes = pyaes.AESModeOfOperationCTR(aesKey.encode())
        dtext = self.aes.decrypt(mes.message)
        print(dtext.decode().split("|")[0])
        self.receive = dtext.decode().split("|")[0]

    def ReceivedMessage(self):
        return self.receive


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("[Usage]file.py message encriptionPass ClientName port")
    '''
    client = ClientUI(sys.argv[3], sys.argv[1], sys.argv[2])
    if sys.argv[3] != "B":
        client.GetPublicKey("localhost", int(sys.argv[4]))
        print(client.ComputeMessage())
    else:
        client.Server("localhost", int(sys.argv[4]))
        client.StartServ()
    '''
    if sys.argv[1] == "B":
        clientB = ClientUI("B", "message", "passwordAesB")
        clientB.Server("localhost", int(50006))
        clientB.StartServ()
    elif sys.argv[1] == "A":
        clientA = ClientUI("A", "Message for B", "passwordAesA")
        clientA.GetPublicKey("localhost", int(50006))
        message = clientA.ComputeMessage()
        print("Compute Mes A: ", message)
        clientA.SendCentral("localhost", int(50007), "message:".encode()+message)

