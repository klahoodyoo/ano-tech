import unittest
import logging
import aspectlib


class Client:
    def __init__(self, address):
        self.address = address
        self.connect()

    def connect(self):
        pass

    def action(self, data):
        pass


def retry(retries=(1, 5, 15, 30, 60), retry_on=(IOError, OSError), prepare=None):
    assert len(retries)

    @aspectlib.Aspect
    def retry_aspect(*args, **kwargs):
        dureations = retries
        while True:
            try:
                yield aspectlib.Proceed
                break
            except retry_on as exc:
                if dureations:
                    logging.warn(exc)
                    time.sleep(dureations[0])
                    dureations = dureations[1:]
                    if prepare:
                        prepare(*args, **kwargs)
                else:
                    raise
        return retry_aspect


# ----------------testing-------------------
'''class Foo:
    def stuff(self, data):
        return "wefw"


class MyTestcase(unittest.TestCase):
    def test_stuff(self):
        @aspectlib.Aspect
        def mock_stuff(self, value):
            if value == 'special':
                yield aspectlib.Return("mocked-result")
            else:
                yield aspectlib.Proceed

        with aspectlib.weave(Foo.stuff, mock_stuff):
            obj = Foo()
            self.assertEqual(obj.stuff("speciall"), 'mocked-result')
'''

# --------------lab7_example from course-------------
class List(list):
    def __init__(self):
        self.list = list()

    def __add__(self, other):
        self.list.append(other)

    def next(self, index):
        return self.list[index]

    def hasNext(self, index):
        if (len(self.list) - 1) > index:
            return 1
        return 0


class Custom:
    def fct(self):
        @aspectlib.Aspect
        def custom_fct(self, name):
            print(type(name))
            if name == "__getitem__":
                print("getitem")
                yield aspectlib.Return("error")
            else:
                yield aspectlib.Proceed

        with aspectlib.weave(List.next, custom_fct):
            obj = List()
            obj.append(10)
            obj.append(12)
            print(obj.next(0))


if __name__ == "__main__":
    # aspectlib.weave(Client, aspectlib.contrib.retry())

    # aspectlib.weave(Client, aspectlib.contrib.retry(prepare=lambda self, *_: self.connect())

    # aspectlib.weave(Client.action, aspectlib.contrib.retry())
    # unittest.main()
    c = Custom()
    c.fct()
