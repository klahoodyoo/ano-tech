import pyaes
import aspectlib


class List:
    def __init__(self):
        self.list = list()

    def Next(self, index):
        return self.list[index]

    def hasNext(self, index):
        if len(self.list) > index:
            return 1
        return 0

    def add(self, item):
        self.list.append(item)


# -----------1.--------------
calledhasNext = 0


def observeNext(obj):
    @aspectlib.Aspect
    def obs(lst, index):
        global calledhasNext
        if not calledhasNext:
            yield aspectlib.Return("error")
        else:
            calledhasNext = 0
            yield aspectlib.Proceed

    return obs


def observeHasNext(obj):
    @aspectlib.Aspect
    def obs(lst, index):
        global calledhasNext
        calledhasNext = 1
        yield aspectlib.Proceed

    return obs


# -------------2.-----------------

flagg = 0


class AesAlg:
    def __init__(self, privKey):
        self.aes = pyaes.AESModeOfOperationCTR(privKey.encode())

    def Encrypt(self, message):
        return self.aes.encrypt(message)

    def Decrypt(self, message):
        return self.aes.decrypt(message)

    def Send(self, data):
        print("Send")
        return 1


def observeBeforeSend(obj):
    @aspectlib.Aspect
    def verify(a, b):
        global flagg
        if flagg == 0:
            yield aspectlib.Return("error")
        else:
            flagg = 0
            yield aspectlib.Proceed

    return verify


def observeAfterDecrypt(obj):
    @aspectlib.Aspect
    def verify(a, b):
        global flagg
        flagg = 1
        yield aspectlib.Proceed

    return verify


if __name__ == "__main__":
    aspectlib.weave(List.Next, observeNext(lambda self, *_: self.Next(_)))
    aspectlib.weave(List.hasNext, observeHasNext(lambda self, *_: self.hasNext(_)))

    lst = List()
    lst.add(10)
    #lst.hasNext(0)
    print(lst.Next(0))

    '''aspectlib.weave(AesAlg.Send, observeBeforeSend(lambda self, *_: self.Send(_)))
    aspectlib.weave(AesAlg.Decrypt, observeAfterDecrypt(lambda self, *_: self.Decrypt(_)))

    aes = AesAlg('UQLORMEEAABA`^UOPK[DE`EMZNAFEHHW')
    criptotext = aes.Encrypt("herbfgerjhgwerjgerkgnerjke")
    # decrypted = aes.Decrypt(criptotext)
    # print(aes.Send("something"))
    print(aes.Decrypt(criptotext))
    print(aes.Send("something"))'''
