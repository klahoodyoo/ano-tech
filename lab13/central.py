import pyaes
import rsa
import pickle
import sys
import socket
import threading
from random import shuffle, choices


def genKey():
    l = [chr(_) for _ in range(32, 127)]
    return "".join(choices(l, k=32))


# used to send to the first node in the network
class Client:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def send(self, message="info"):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        s.connect((self.ip, self.port))

        if type(message) == str:
            message = message.encode()

        s.send(message)
        data = s.recv(4096)
        s.close()
        print("[central]received client central: ", data)
        if data[:8] == "received".encode():
            return 1
        return 0


# receive the public keys of nodes and encrypt the message from client
class Server:
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.nodes = list()
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))
        self.aesKeys = list()

        self.messages = list()
        self.pubKeys = dict()

    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            if address:
                print("[central]Connected", address)
            client.settimeout(60)
            threading.Thread(target=self.listenToClient, args=(client, address)).start()

    def listenToClient(self, client, address):
        data = self.readpacket(client)

        # try:
        print("[central]data", data)
        # print("pubKey", self.pubKey)
        if "pubKeys:".encode() == data[:8]:  # received all public keys from nodes
            # print("----------------", pickle.loads(data[8:]))
            ipnode = pickle.loads(data[-40:].split("|".encode())[1])
            print("=========", ipnode)
            self.nodes.append(ipnode)  # list of ip nodes
            print("nodes", self.nodes)
            # print("[central]--------------", ipnode)
            self.pubKeys[ipnode] = pickle.loads(data[8:])
            client.send("received".encode())
            # print("Sended!")
        elif "message:".encode() == data[:8]:  # received encrypted message
            print(self.pubKeys.keys())
            mes = pickle.loads(data[8:])
            print("[central]message received", mes.message)
            self.messages.append(data[8:])
            client.sendall("received".encode())

            toSend, ip = self.computeMessage(data[8:])

            m = pickle.loads(toSend)
            print("[central]message", m.message)
            print("[central]ip", ip)
            r = self.sendover(ip, "packet:".encode() + toSend)
            if r:
                print("[central]Message sent to node")
        elif "closeS".encode() == data[:6]:
            print("-"*25)
            print(self.nodes)
            print(self.pubKeys)
            print("-" * 25)
            ip = data[6:].split(":".encode())
            print((ip[0].decode(), int(ip[1],)))
            self.nodes.remove((ip[0].decode(), int(ip[1],)))
            del self.pubKeys[(ip[0].decode(), int(ip[1],))]
            print("-" * 25)
            print(self.nodes)
            print(self.pubKeys)
            print("-" * 25)
            client.sendall("received".encode())

        client.close()
        return True
        '''except Exception as err:
            print("serv err", err)
            client.close()
            return False'''

    def readpacket(self,client):
        size = 4096
        data = b""
        while True:
            chunk = client.recv(size)
            data += chunk
            if len(chunk) < size: break
        return data

    def generateAesKeys(self):
        for i in range(len(self.nodes)):
            self.aesKeys.append(genKey())

    def computeMessage(self, toSend):
        lastKey, lastValue = list(self.pubKeys.keys())[-1], self.pubKeys[list(self.pubKeys.keys())[-1]]
        copyPubKeys = list(self.pubKeys.keys())
        final = bytes()
        flag = True

        # generate random aes keys
        self.generateAesKeys()

        for ii, i in zip(self.aesKeys, copyPubKeys):
            aes = pyaes.AESModeOfOperationCTR(ii.encode())
            if flag:
                ddd = pickle.loads(toSend)
                print("-------compute", ddd.message)
                cAes = aes.encrypt(toSend)
                flag = False
            else:
                cAes = aes.encrypt(final)

            print("--------ip", i)
            print("--------key", self.pubKeys[i])

            cAes = cAes + "|".encode() + i[0].encode() + ":".encode() + str(i[1]).encode()
            cKey = rsa.encrypt(ii.encode(), self.pubKeys[i])
            final = pickle.dumps(Message(cAes, cKey), 0)

        '''aes = pyaes.AESModeOfOperationCTR(self.aesKeys[-1])
        cAes = aes.encrypt(final)
        cKey = rsa.encrypt(self.aesKeys[-1].encode(), lastValue)
        final = pickle.dumps(Message(cAes, cKey), 0)'''

        return final, lastKey

    def Stop(self):
        self.sock.close()

    def sendover(self, address, data):
        cl = Client(address[0], int(address[1]))
        return cl.send(data)


class Message:
    def __init__(self, message, cKey):
        self.message = message
        self.cKey = cKey


if __name__ == "__main__":
    central = Server("localhost", 50007)
    '''for _ in range(1, 4):
        pub, priv = rsa.newkeys(1024)
        central.pubKeys[("localhost", 50007 + _)] = pub'''

    central.listen()
