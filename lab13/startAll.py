# subprocess.call(["python", "mainClient.py", "B"])
# subprocess.call(["python", "nodes.py", str(50008 + 1)])
# subprocess.call(["python", "central.py"])

from subprocess import Popen
from time import sleep

DETACHED_PROCESS = 0x00000008

# central 50007
srvCentral = Popen(["python.exe", "central.py"], creationflags=DETACHED_PROCESS, shell=True)

# nodes 50008 50009 50010
for i in range(1, 4):
    Popen(['python.exe', 'nodes.py', str(50007 + i)], creationflags=DETACHED_PROCESS, shell=True)

# -----------------TDD-------------------------
clientB = Popen(["python.exe", "mainClient.py", "B", "50011"], creationflags=DETACHED_PROCESS, shell=True)
# ------------------------------------------
'''
# --------------------------NON Functional 1------------------------------
clientB1 = Popen(["python.exe", "mainClient.py", "B", "50011"], creationflags=DETACHED_PROCESS, shell=True)
sleep(5)
clientA1 = Popen(["python.exe", "mainClient.py", "A", "50011"], creationflags=DETACHED_PROCESS, shell=True)
sleep(5)
clientA2 = Popen(["python.exe", "mainClient.py", "A", "50011"], creationflags=DETACHED_PROCESS, shell=True)
sleep(5)
clientA3 = Popen(["python.exe", "mainClient.py", "A", "50011"], creationflags=DETACHED_PROCESS, shell=True)
# ------------------------------------------------------------------------

# --------------------------NON Functional 2------------------------------
clientB1 = Popen(["python.exe", "mainClient.py", "B", "50011"], creationflags=DETACHED_PROCESS, shell=True)
sleep(5)
clientA1 = Popen(["python.exe", "mainClient.py", "A", "50011"], creationflags=DETACHED_PROCESS, shell=True)

clientB2 = Popen(["python.exe", "mainClient.py", "B", "50012"], creationflags=DETACHED_PROCESS, shell=True)
sleep(5)
clientA2 = Popen(["python.exe", "mainClient.py", "A", "50012"], creationflags=DETACHED_PROCESS, shell=True)

clientB3 = Popen(["python.exe", "mainClient.py", "B", "50013"], creationflags=DETACHED_PROCESS, shell=True)
sleep(5)
clientA3 = Popen(["python.exe", "mainClient.py", "A", "50013"], creationflags=DETACHED_PROCESS, shell=True)
# -------------------------------------------------------------------------
'''