import unittest
from mainClient import *
from nodes import *


'''
--------TDD-------
test 1 - A received pubKey from B
test 2 - central receive pubkey from node x
test 3 - central remove node x when node is offline
test 4 - B received message from A

to imp: when last node send to B, notifies central server. if true ok else 
        reconstruct the message and interogate all nodes isAlive

--------Non-functional--------
performance-testing - can handle large quantitties of data
load-testing - large amount of users
usability testing - interface is easy to use and understand

'''

from subprocess import Popen

DETACHED_PROCESS = 0x00000008


class TestMethods(unittest.TestCase):
    '''def test_A_received_pubkey_B(self):
        clientB = Popen(["python.exe", "mainClient.py", "B"], creationflags=DETACHED_PROCESS, shell=True)

        clientA = ClientUI("A", "Message for B", "passwordAesA")
        clientA.GetPublicKey("localhost", int(50006))
        message = clientA.ComputeMessage()
        self.assertIsNotNone(message)
        '''

    '''def test_central_receive_pubKey_nodeX(self):
        # comment in nodes Server=> __init__ ----> self.sendpubKey(centralAddress)
        srvCentral = Popen(["python.exe", "central.py"], creationflags=DETACHED_PROCESS, shell=True)

        # Popen(['python.exe', 'nodes.py', str(50007 + i)], creationflags=DETACHED_PROCESS, shell=True)
        server = Server("localhost", int(50008), ("localhost", int(50007)))
        res = server.sendpubKey(("localhost", int(50007)))
        self.assertEqual(res, True)
        # server.listen()'''

    def test_central_remove_nodeX(self):
        srvCentral = Popen(["python.exe", "central.py"], creationflags=DETACHED_PROCESS, shell=True)
        # node1 = Popen(['python.exe', 'nodes.py', str(50008)], creationflags=DETACHED_PROCESS, shell=True)
        # clientB = Popen(["python.exe", "mainClient.py", "B"], creationflags=DETACHED_PROCESS, shell=True)
        server = Server("localhost", int(50008), ("localhost", int(50007)))
        res = server.shutdownServer()
        self.assertEqual(res, True)

    def test_B_receive_message_fromA(self):
        pass


if __name__ == '__main__':
    unittest.main()
