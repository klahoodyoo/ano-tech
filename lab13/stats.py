from subprocess import Popen
from time import sleep
import mainClient as main
import cProfile as profiler
from pycallgraph import PyCallGraph
from pycallgraph.output import GraphvizOutput
from mainClient import *
from nodes import *

DETACHED_PROCESS = 0x00000008

if __name__ == "__main__":

    graphviz = GraphvizOutput()

    graphviz.output_file = 'stats/stats_open_server.png'
    with PyCallGraph(output=graphviz):
        central = Popen(["python.exe", "central.py"], creationflags=DETACHED_PROCESS, shell=True)
        procs = []
        # nodes 50008 50009 50010
        for i in range(1, 4):
            procs.append(Popen(['python.exe', 'nodes.py', str(50007 + i)], creationflags=DETACHED_PROCESS, shell=True))

        b = Popen(["python.exe", "mainClient.py", "B", "50011"], creationflags=DETACHED_PROCESS, shell=True)

    graphviz.output_file = 'stats/stats_send.png'
    with PyCallGraph(output=graphviz):
        clientA = main.ClientUI("A", "Message for B", "passwordAesA")
        clientA.GetPublicKey("localhost", 50011)  # 50006
        message = clientA.ComputeMessage()
        clientA.SendCentral("localhost", int(50007), "message:".encode() + message)

    graphviz.output_file = 'stats/stats_send_and_receive.png'
    with PyCallGraph(output=graphviz):
        b = Popen(["python.exe", "mainClient.py", "B", "50011"], creationflags=DETACHED_PROCESS, shell=True)
        clientA = main.ClientUI("A", "Message for B", "passwordAesA")
        clientA.GetPublicKey("localhost", 50011)  # 50006
        message = clientA.ComputeMessage()
        clientA.SendCentral("localhost", int(50007), "message:".encode() + message)

    graphviz.output_file = 'stats/stats_shutdown.png'
    with PyCallGraph(output=graphviz):
        srvCentral = Popen(["python.exe", "central.py"], creationflags=DETACHED_PROCESS, shell=True)
        server = Server("localhost", int(50008), ("localhost", int(50007)))
        res = server.shutdownServer()

    graphviz.output_file = 'stats/stats_receive_public_key.png'
    with PyCallGraph(output=graphviz):
        clientB = Popen(["python.exe", "mainClient.py", "B", "50011"], creationflags=DETACHED_PROCESS, shell=True)
        clientA = ClientUI("A", "Message for B", "passwordAesA")
        clientA.GetPublicKey("localhost", int(50011))
        message = clientA.ComputeMessage()