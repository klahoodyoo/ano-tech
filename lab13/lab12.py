import rsa

pub, priv = rsa.newkeys(512, exponent=3)

print("pub", pub)
print(pub._save_pkcs1_pem())
print("priv", priv)

enc = rsa.encrypt(b"msg", pub)
f = open("enc.txt", "wb")
f.write(enc)
f.close()

f = open("enc_pub.txt", "wb")
f.write(pub._save_pkcs1_pem())
f.close()

# http://m0x39.blogspot.com/2012/12/0x00-introduction-this-post-is-going-to.html
# https://en.wikipedia.org/wiki/Onion_routing
# https://ritter.vg/blog-mix_and_onion_networks.html
