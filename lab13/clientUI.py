import sys
import os
from PyQt5 import QtCore, QtGui, uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QFileDialog, QMessageBox
from ui import Ui_MainWindow
import pyaes
import rsa
from os import urandom
import pickle
import sys
import socket
import threading
from random import choices

class Client:
    def __init__(self, ip, port):
        self.ip = ip
        self.port = port

    def send(self, message="info".encode()):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        s.connect((self.ip, self.port))
        s.send(message)
        data = s.recv(1024)
        s.close()
        print("received", data)
        if data[:5] == "info:".encode():
            return data[5:]
        elif data == "received".encode():
            return data
        else:
            return ""


def padding(mes):
    return mes + ('_' * (32 - len(mes)))


def genKey():
    l = [chr(_) for _ in range(32, 127)]
    return "".join(choices(l, k=32)).encode()


class Message:
    def __init__(self, message, cKey):
        self.message = message
        self.cKey = cKey


class ClientUI:
    def __init__(self, name, message, privKeymes):
        self.name = name
        self.privMessage = message
        self.aes = pyaes.AESModeOfOperationCTR(padding(privKeymes).encode())
        self.pubKey, self.privKey = rsa.newkeys(1024)
        # self.address = "localhost:" + str(port)
        self.otherClientAddr = str()
        self.otherPubKey = str()
        self.privKMess = privKeymes

        self.receive = str()

    def GetPublicKey(self, ip, port):
        self.otherClientAddr = ip + ":" + str(port)
        self.otherPubKey = Client(ip, port).send()
        print("[client]otherPublicKey", pickle.loads(self.otherPubKey))

    def ComputeMessage(self):
        r = urandom(10)
        cAes = self.aes.encrypt(self.privMessage)
        cAes = cAes + "|".encode() + self.otherClientAddr.encode()
        cKey = rsa.encrypt(self.privKMess.encode(), pickle.loads(self.otherPubKey))
        ctext = pickle.dumps(Message(cAes, cKey), 0)  # what b receives

        # print("b mes:", ctext, "len: ", len(ctext))
        # ctext = ctext + b"\x00" * padding(ctext)
        # print("aaa", len(ctext))

        return ctext

    def SendCentral(self, ip, port, data):
        response = Client(ip, port).send(data)
        if len(response):
            print("Send to central server")
        else:
            print("Not sent to central server")

    def PublicKey(self):
        return self.pubKey

    def GetInfoClient(self, client):
        self.otherClientAddr = client.address
        self.otherPubKey = client.PublicKey()

    def Receive(self, request):
        mes = pickle.loads(request)
        print("Client B received:", mes.message, "_____", mes.cKey)
        aesKey = self.alg.Decrypt(mes.cKey, self.privKey)
        print(aesKey)
        self.aes = pyaes.AESModeOfOperationCTR(padding(aesKey.decode).encode())
        dtext = self.aes.decrypt(mes.message)
        print(dtext.decode().split("|")[0])
        self.receive = dtext.decode().split("|")[0]

    def ReceivedMessage(self):
        return self.receive


class MyApp(QMainWindow, Ui_MainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        Ui_MainWindow.__init__(self)
        self.setupUi(self)

        self.pushButton.clicked.connect(self.exitApp)
        self.pushButton_2.clicked.connect(self.send)

        # self.textEdit.textChanged.connect(self.textChanged1)

        # self.lineEdit.textChanged.connect(self.linetxtChanged1)

    def exitApp(self):
        exit(1)

    def send(self):
        print("send")
        if len(self.textEdit.toPlainText()) > 0:
            clientA = ClientUI("A", self.textEdit.toPlainText(), "passwordAesA")
            ip = self.lineEdit.text().split(":")
            print(ip)
            clientA.GetPublicKey(ip[0], int(ip[1]))  # was 50006
            message = clientA.ComputeMessage()
            print("Compute Mes A: ", message)
            clientA.SendCentral("localhost", int(50007), "message:".encode() + message)

    def textChanged1(self):
        if len(self.textEdit.toPlainText()) > 0:
            self.pushButton_3.setEnabled(True)
        else:
            self.pushButton_3.setEnabled(False)
            self.pushButton_5.setEnabled(False)

    def linetxtChanged1(self):
        if not self.lineEdit.text():
            self.pushButton_2.setEnabled(False)
        else:
            self.pushButton_2.setEnabled(True)

    def showDialogFileSamples(self):
        try:
            fname = QFileDialog.getOpenFileName(self, 'Open file', '.')
            if os.path.exists(fname[0]):
                self.lineEdit_3.setText(fname[0])
            else:
                QMessageBox.information(self, "Error", "Invalid input file")

        except Exception as err:
            print(str(err))


        # QMessageBox.information(self, "Found", "Wrriten to log file")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    window = MyApp()
    window.show()
    sys.exit(app.exec_())