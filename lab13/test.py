import unittest

# from allp import *

'''
--------TDD-------
test 1 - A received pubKey from B
test 2 - central receive pubkey from node x
test 3 - central remove node x when node is offline
test 4 - B received message from A

to imp: when last node send to B, notifies central server. if true ok else 
        reconstruct the message and interogate all nodes isAlive
        
--------Non-functional--------
performance-testing - can handle large quantitties of data
load-testing - large amount of users
usability testing - interface is easy to use and understand

'''


class myTestt(unittest.TestCase):
    def test(self):
        self.assertEqual(4, 4)

    def test2(self):
        self.assertEqual(4, 5)


class myTest(unittest.TestCase):
    def test(self):
        from random import choice

        ports = [_ for _ in range(5006, 5016)]
        servs = [Server() for _ in ports]
        [_.SetIpPort("localhost", port) for _, port in zip(servs, ports)]
        alg = RsaAlg()
        nods = [Node("name " + str(_), serv, alg) for _, serv in zip(range(len(ports)), servs)]

        for i in nods:
            i.server.Start()

        temp = nods[:-1]
        for i in nods[:-1]:
            toTake = choice(temp)
            i.neighbours = [toTake]
            temp.remove(toTake)
        print([_.neighbours[0].name for _ in nods[:-1]])

        clientA = Client("A", "Hello B", 7000, "private_key_for_B" + "_" * 15)
        # clientB = Client("B", "", 6000, "private_key_for_A" + "_" * 15)

        clientA.GetInfoClient(clientB)
        clientA.SetFirstNode(nods[-1])
        clientA.Send()

        self.assertEqual("Hello B", clientB.ReceivedMessage())


class TestAes(unittest.TestCase):
    def test(self):
        rsaAlg = RsaAlg()
        ctext = rsaAlg.Encrypt("message1".encode(), rsaAlg.GetPubKey())

        self.assertEqual("message1", rsaAlg.Decrypt(ctext, rsaAlg.GetPrivKey()))


if __name__ == "__main__":
    unittest.main()
